// tap.js returns necessary functions and variables in the Qualtrics header

// return rounded floating-point numbers to give an impression of precision
function rounder(value) {
  value = +value;
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] - -2) : 2)));
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] + -2) : -2));
};

// return Qualtrics PipeText string from arguments
function qualtricsPipeTextStringifier(syntax, identifier, question, multi) {
  if (multi == true) {
    var i = 1
    var qualtricsPipeTextStrings = {}
    while (i < 3) {
      qualtricsPipeTextStrings["qualtricsPipeTextString" + i] = '${' + syntax + '://' + i + '_' + identifier + '/' + question + '}';
      i = i + 1;
    }
    return qualtricsPipeTextStrings
  } else {
    var qualtricsPipeTextString = '${' + syntax + '://' + identifier + '/' + question + '}';
    return qualtricsPipeTextString;
  }
};

// return variable as floating point if is "q://" syntax, int if not 
function qualtricsPipeTextEvaluator(qualtricsPipeTextString) {
  if (typeof qualtricsPipeTextString == "object") {
     var total = parseFloat(qualtricsPipeTextString["qualtricsPipeTextString1"]) + parseFloat(qualtricsPipeTextString["qualtricsPipeTextString2"]) + parseFloat(qualtricsPipeTextString["qualtricsPipeTextString3"]);
     return rounder(total)
  } else {
    return parseInt(qualtricsPipeTextString);
  }
};

// switch color from green to red
function switchTextColor() {
  var textToSwitch = document.getElementById("textToSwitch");
  textToSwitch.style.color = "red";
}

// blast user with airhorn noise at volume of param1 (v) for duration of param2 (d)
function blast(v,d) {
  var wavLink = "https://bitbucket.org/michaelnetbiz/tap/raw/491609de2486a0095229dd3f0150f02f56f91d1d/airhorn" + d + ".wav"
  var audiobject = new Audio(wavLink);
  audiobject.volume = v / 10;
  audiobject.play()
}

// render
function pipeTextRenderer(){
  
};

/*
e.g., when reporting to the user how long they took on the practice quiz,
with pipeTextParser declared in the header, the specific item JavaScript need
only read:

pipeTextComposer("Q1D7", "q", "ChoiceNumericEntryValue");

Return a floating point number representing the length take to submit a question
*/